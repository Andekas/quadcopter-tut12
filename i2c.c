/*************************************************************************
 *
 *    Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2003
 *
 *    File name   : main.c
 *    Description : Define main module
 *
 *    History :
 *    1. Date        : Feb 10, 2005
 *       Author      : Stanimir Bonev
 *       Description : Create
 *
 * Buttons function
 * Butt1 - Next
 * Butt2 - Select
 *
 * Terminal settings:
 * UART0
 * ------------------
 * Communication speed: 9600 bps
 * Number of data bits:    8
 * Number of stop bits:    1
 * Parity:              none
 *
 * UART1
 * ------------------
 * Communication speed: 9600 bps
 * Number of data bits:    8
 * Number of stop bits:    1
 * Parity:              none
 *
 * Jumper settings:
 * ----------------
 *
 *
 * Set PWM/DAC to DAC.
 * Set Ext/J-Linck to Ext.
 *
 *
 *
 *    $Revision: 47021 $
**************************************************************************/
#include "includes.h"
#include <user_func.h>

#define LIGHT_ALWAYS_OFF  0
#define LIGHT_AUTO        1
#define LIGHT_ALWAYS_ON   2
#define LIGHT_AUTO_OFF_TO TICK_PER_SECOND*10 /* 10sec */

#define GET_TIME          '1'
#define GET_DATE          '2'

#define R_bit           0x01
#define W_bit           0x00
#define GYRO_ADR        0x68
#define GYRO_XOUT_ADR   0x1D

const char UART_Menu[] = "\r\nUart commands\r\n'1' Get Time\r\n'2' Get Date\r\n'?' Help\n\r";

volatile int TickSysFlag = 0;
int SysTimeUpdateFlag = 0;
int SysAlarmFlag = 0;

int B1_Short = 0;
int B2_Short = 0;

int TimeFormat = 2;
int DataFormat = 1;

int LightMode = LIGHT_AUTO;

/*************************************************************************
 * Function Name: irq_handler
 * Parameters: void
 * Return: void
 *
 * Description: IRQ subroutine
 *		
 *************************************************************************/
__irq __arm void irq_handler (void)
{
void (*interrupt_function)();
unsigned int vector;

  vector = VICVectAddr;     // Get interrupt vector.
  interrupt_function = (void(*)())vector;
  (*interrupt_function)();  // Call vectored interrupt function.
}

/*************************************************************************
 * Function Name: fiq_handler
 * Parameters: void
 * Return: void
 *
 * Description: FIQ subroutine
 *		
 *************************************************************************/
__fiq __arm void fiq_handler (void)
{
void (*interrupt_function)();
unsigned int vector;

  vector = VICVectAddr;     // Get interrupt vector.
  interrupt_function = (void(*)())vector;
  (*interrupt_function)();  // Call vectored interrupt function.
}

/*************************************************************************
 * Function Name: NonVectISR
 * Parameters: void
 * Return: void
 *
 * Description: non vectored callback subroutine
 *		
 *************************************************************************/
void NonVectISR(void)
{
}

/*************************************************************************
 * Function Name: ClearFlag
 * Parameters: void
 * Return: void
 *
 * Description: clear arg
 *		
 *************************************************************************/
void ClearFlag (void* arg)
{
int* pFlag = arg;
  *pFlag = 0;
}

/*************************************************************************
 * Function Name: DelayResolution100us
 * Parameters: Int32U Delay
 * Return: void
 *
 * Description: Timer1 CH0 subroutine - delay [100us]
 *		
 *************************************************************************/
void DelayResolution100us(Int32U Delay)
{
volatile int Flag = 1;
  // Stop Timer 1
  TIMER_Stop(TIMER1);
  // Stop Reset Timer 1 counter
  TIMER_Reset(TIMER1);
  // Set action of match module CH0
  TIMER_SetMatchAction(TIMER1, CH0, TimerAction_Interrupt | TimerAction_StopTimer,
  Delay usec_T1*100, ClearFlag, (void *)&Flag, DONOTHING);
  // Start Timer 1
  TIMER_Start(TIMER1);
  // Wait expire of delay
  while(Flag);
}

/*************************************************************************
 * Function Name: SysInit
 * Parameters: void
 * Return: int
 *
 * Description: Hardware initialize
 *		
 *************************************************************************/
int SysInit(void)
{
  // Initialize the system
#ifdef FLASH
  if (SYS_Init(FOSC, FCCLK, VPBDIV1, USER_FLASH, 0x0001FF0F,0x87FE01F1,0,0xFFFFFFFF))
    return 1;
#else
  if (SYS_Init(FOSC, FCCLK, VPBDIV1, USER_RAM,   0x0001FF0F,0x87FE01F1,0,0xFFFFFFFF))
    return 1;
#endif
  // Initialize Serial Interface
  if (UART_Init(UART0))
    return 1;
  if (UART_Init(UART1))
    return 1;

  // Initialize Timers
  PM_OpenPeripheral(PC_TIMER0);
  if (TIMER_Init(TIMER0, TIMER_PRECISION))
    return 1;
  PM_OpenPeripheral(PC_TIMER1);
  if (TIMER_Init(TIMER1, TIMER_PRECISION))
    return 1;

  // Initialize RTC
  if (RTC_Init(0))	
    return 1;

  // initialize VIC
  VIC_Init();
  VIC_SetProtectionMode(UserandPrivilegedMode);
  // Enable interrupts non vectored interrupts
  VIC_EnableNonVectoredIRQ(NonVectISR);

  // UART0 interrupt
  VIC_SetVectoredIRQ(UART0_ISR,VIC_Slot0,VIC_UART0);
  VIC_EnableInt(1<<VIC_UART0);

  // UART1 interrupt
  VIC_SetVectoredIRQ(UART1_ISR,VIC_Slot1,VIC_UART1);
  VIC_EnableInt(1<<VIC_UART1);

  // Timer0 interrupt
  VIC_SetVectoredIRQ(TIMER0_ISR,VIC_Slot2,VIC_TIMER0);
  VIC_EnableInt(1<<VIC_TIMER0);

  // Timer1 interrupt
  VIC_SetVectoredIRQ(TIMER1_ISR,VIC_Slot3,VIC_TIMER1);
  VIC_EnableInt(1<<VIC_TIMER1);

  // UART1 interrupt
  VIC_SetVectoredIRQ(UART1_ISR,VIC_Slot4,VIC_UART1);
  VIC_EnableInt(1<<VIC_UART1);

  // RTC interrupt
  VIC_SetVectoredIRQ(RTC_ISR,VIC_Slot5,VIC_RTC);
  VIC_EnableInt(1<<VIC_RTC);

  UART_PutStringByPolling(UART0, "\n\rLPC2148 Hardware Init Finish!\n\r");
  UART_PutStringByPolling(UART0, "======Start User Program======\n\r");
  UART_PutStringByPolling(UART1, "\n\rLPC2148 Hardware Init Finish!\n\r");
  UART_PutStringByPolling(UART1, "======Start User Program======\n\r");
  return 0;
}

/*************************************************************************
 * Function Name: UserStart
 * Parameters: void
 * Return: void
 *
 * Description: User demo subroutine
 *		
 *************************************************************************/
void UserStart(void)
{
  /* System time init */
  TIMER_SetMatchAction(TIMER0, CH0, TimerAction_Interrupt | TimerAction_ResetTimer,
  1sec_T0/TICK_PER_SECOND, SetSysTickFlag, (void *)&TickSysFlag, DONOTHING);
  TIMER_Start(TIMER0);
  /* HD44780 LCD driver init */
  HD44780_PowerUpInit();
  /* Buttons Init */
  ButtonsInit();
  /* Light Init */
  LightInit();
  /* Menu Init */
  MenuInit(MENU_ENGLISH_LANG,MENU_WELCOME_SHOW,NULL,MENU_WELCOM_DLY);
  /* RTC_Enable */
  RTC_Enable();
}

/*************************************************************************
 * Function Name: main
 * Parameters: void
 * Return: void
 *
 * Description: Main subroutine
 *		
 *************************************************************************/

void i2c_init(void)
{
  //pin configuration
  
  //I2C0 SDA0 and SCL0
  PINSEL0_bit.P0_3=0x01;
  PINSEL0_bit.P0_2=0x01;
  //LED pins
  IO0DIR_bit.P0_10 = 1;
  IO0DIR_bit.P0_11 = 1;
   
 
   //clear all
  I2C0CONCLR_bit.AAC=1;
  I2C0CONCLR_bit.I2ENC=1;
  I2C0CONCLR_bit.SIC=1;
  I2C0CONCLR_bit.STAC=1;

  //i2c init 
   I2C0CONSET_bit.I2EN=1;
  I2C0CONSET_bit.STA=0;
  I2C0CONSET_bit.STO=0;
  I2C0CONSET_bit.SI=0;
  I2C0CONSET_bit.AA=0;
  
  //set i2c rate
  I2C0SCLL_bit.COUNT=400;
  I2C0SCLH_bit.COUNT=400;
}

void delay(int aeg)
{
  for(int i = 0;i < 1000*aeg;i++);
}
void set_master_transmit(unsigned char SLA, unsigned char data[])
{
  //Begin a Master Receive/Transmit operation by setting up the buffer, pointer, and data count
unsigned char dataCount = 1;
unsigned char dataPointer = 0;
unsigned char transmitBuffer[8] = {0,0,0,0,0,0,0,0};
//unsigned char dataBytes = 1;
unsigned char slaveAddress = 0x00;       //7-bit slave address (+ R/W bit)
//1. Initialize Master data counter.
    dataCount = 1;
//2. Set up the Slave Address to which data will be transmitted, and add the Write bit.
    slaveAddress = SLA | W_bit;
//3. Write 0x20 to I2C0CONSET to set the STA bit.
    I2C0CONSET = 0x20;
//4. Set up data to be transmitted in Master Transmit buffer.
    for(unsigned char i = 0; i < sizeof(data)/4*sizeof(unsigned char); i++)
    {
        transmitBuffer[i] = data[i];
    }
//5. Initialize the Master data counter to match the length of the message being sent.
    dataCount = sizeof(data)/4*sizeof(unsigned char);
	// i2c interrupt routine
	while(dataCount)
	{
          delay(0);
		switch(I2C0STAT_bit.STATUS)
		{
			case 0x00:
				//Bus Error. Enter not addressed Slave mode and release bus.
				
				//1. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			case 0x08:
				//A Start condition has been transmitted. The Slave Address + R/W bit will be transmitted,
				//an ACK bit will be received.
			
				//1. Write Slave Address with R/W bit to I2DAT.
				I2C0DAT = slaveAddress;
                                while(I2C0DAT != slaveAddress){ delay(0);}
				//2. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Set up Master Transmit mode data buffer.
				//5. Set up Master Receive mode data buffer.
				//6. Initialize Master data counter.
				dataCount = sizeof(data)/sizeof(unsigned char);
				//7. Exit
				break;
			case 0x10:	
				//A repeated Start condition has been transmitted. The Slave Address + R/W bit will be
				//transmitted, an ACK bit will be received.
			
				//1. Write Slave Address with R/W bit to I2DAT.
				I2C0DAT_bit.DATA = slaveAddress;
				//2. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Set up Master Transmit mode data buffer.
				//5. Set up Master Receive mode data buffer.
				//6. Initialize Master data counter.
				dataCount = sizeof(data)/sizeof(unsigned char);
				//7. Exit
				break;
			case 0x18:
				//Previous state was State 8 or State 10, Slave Address + Write has been transmitted, ACK
				//has been received. The first data byte will be transmitted, an ACK bit will be received.
			
				//1. Load I2DAT with first data byte from Master Transmit buffer.
				I2C0DAT_bit.DATA = transmitBuffer[dataPointer];
				//2. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Increment Master Transmit buffer pointer.
				dataPointer++;
				//5. Exit
				break;
			case 0x20:
				//Slave Address + Write has been transmitted, NOT ACK has been received. A Stop
				//condition will be transmitted.
			
				//1. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			case 0x28:
				//Data has been transmitted, ACK has been received. If the transmitted data was the last
				//data byte then transmit a Stop condition, otherwise transmit the next data byte.
				
				//1. Decrement the Master data counter, skip to step 5 if not the last data byte.
				dataCount--;
				if(!dataCount)
				{
					//2. Write 0x14 to I2C0CONSET to set the STO and AA bits.
					I2C0CONSET = 0x14;
					//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
					I2C0CONCLR = 0x28;
					//4. Exit
				}else
				{
				//5. Load I2DAT with next data byte from Master Transmit buffer.
				I2C0DAT_bit.DATA = transmitBuffer[dataPointer];
				//6. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Increment Master Transmit buffer pointer.
				dataPointer++;
				}
				//9. Exit
				break;
			case 0x30:
				//Data has been transmitted, NOT ACK received. A Stop condition will be transmitted.
				//1. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			case 0x38:
				//Arbitration has been lost during Slave Address + Write or data. The bus has been
				//released and not addressed Slave mode is entered. A new Start condition will be
				//transmitted when the bus is free again.
				
				//1. Write 0x24 to I2C0CONSET to set the STA and AA bits.
				I2C0CONSET = 0x24;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			default:
				//2. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Exit
				break;
		} //-switch
	} //-while
}
void set_master_recieve(unsigned char SLA, unsigned char length)
{
    //Begin a Master Receive/Transmit operation by setting up the buffer, pointer, and data count
unsigned char dataCount = 1;
unsigned char dataPointer = 0;
unsigned char recieveBuffer[8] = {0,0,0,0,0,0,0,0};
//unsigned char dataBytes = 1;
unsigned char slaveAddress = 0x00;       //7-bit slave address (+ R/W bit)
//1. Initialize Master data counter.
    dataCount = 1;
//2. Set up the Slave Address to which data will be transmitted, and add the Read bit.
    slaveAddress = SLA | R_bit;
//3. Write 0x20 to I2C0CONSET to set the STA bit.
    I2C0CONSET_bit.STA=1;
//4. Set up the Master Receive buffer.
//5. Initialize the Master data counter to match the length of the message to be received.
    dataCount = length;
//6. Exit
	while(dataCount)
	{
		switch(I2C0STAT_bit.STATUS)
		{
			case 0x00:
				//Bus Error. Enter not addressed Slave mode and release bus.
				
				//1. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			case 0x08:
				//A Start condition has been transmitted. The Slave Address + R/W bit will be transmitted,
				//an ACK bit will be received.
			
				//1. Write Slave Address with R/W bit to I2DAT.
				I2C0DAT_bit.DATA = slaveAddress;
				//2. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Set up Master Transmit mode data buffer.
				//5. Set up Master Receive mode data buffer.
				//6. Initialize Master data counter.
				dataCount = length;
				//7. Exit
				break;
			case 0x10:	
				//A repeated Start condition has been transmitted. The Slave Address + R/W bit will be
				//transmitted, an ACK bit will be received.
			
				//1. Write Slave Address with R/W bit to I2DAT.
				I2C0DAT_bit.DATA = slaveAddress;
				//2. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Set up Master Transmit mode data buffer.
				//5. Set up Master Receive mode data buffer.
				//6. Initialize Master data counter.
				dataCount = length;
				//7. Exit
				break;
			case 0x40:
				//Previous state was State 08 or State 10. Slave Address + Read has been transmitted,
				//ACK has been received. Data will be received and ACK returned.
				
				//1. Write 0x04 to I2C0CONSET to set the AA bit.
				I2C0CONSET = 0x04;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			case 0x48:
				//Slave Address + Read has been transmitted, NOT ACK has been received. A Stop
				//condition will be transmitted.
				
				//1. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//2. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//3. Exit
				break;
			case 0x50:
				//Data has been received, ACK has been returned. Data will be read from I2DAT. Additional
				//data will be received. If this is the last data byte then NOT ACK will be returned, otherwise
				//ACK will be returned.
				//1. Read data byte from I2DAT into Master Receive buffer.
				recieveBuffer[dataPointer] = I2C0DAT_bit.DATA;
				//2. Decrement the Master data counter, skip to step 5 if not the last data byte.
				dataCount--;
				if(!dataCount)
				{
					//3. Write 0x0C to I2C0CONCLR to clear the SI flag and the AA bit.
					I2C0CONCLR = 0x0c;
					//4. Exit
					break;
				}else
				{
					//5. Write 0x04 to I2C0CONSET to set the AA bit.
					I2C0CONSET = 0x04;
					//6. Write 0x08 to I2C0CONCLR to clear the SI flag.
					I2C0CONCLR = 0x28;
					//7. Increment Master Receive buffer pointer
					dataPointer++;
					//8. Exit
					break;
				}
			case 0x58:
				//Data has been received, NOT ACK has been returned. Data will be read from I2DAT. A
				//Stop condition will be transmitted.
				
				//1. Read data byte from I2DAT into Master Receive buffer.
				recieveBuffer[dataPointer] = I2C0DAT_bit.DATA;
				//2. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Exit
				break;
			default:
				//2. Write 0x14 to I2C0CONSET to set the STO and AA bits.
				I2C0CONSET = 0x14;
				//3. Write 0x08 to I2C0CONCLR to clear the SI flag.
				I2C0CONCLR = 0x28;
				//4. Exit
				break;
		} //-switch
	} //-while
}

int main(void)
{
    //void set_master_transmit(unsigned char, unsigned char);
    //void set_master_recieve(unsigned char, unsigned char);
    //set_master_transmit(GYRO_ADR << 1, GYRO_XOUT_ADR);
  unsigned char adr = 0x68 << 1;
  unsigned char data[] = {0x1D};
    i2c_init();
    set_master_transmit(adr, data);
    return 0;
}