
#include "includes.h"
#include <user_func.h>
#include "declarations.h"

/*************************************************************************
 * Function Name: irq_handler
 * Parameters: void
 * Return: void
 *
 * Description: IRQ subroutine
 *		
 *************************************************************************/
__irq __arm void irq_handler (void)
{
void (*interrupt_function)();
unsigned int vector;

  vector = VICVectAddr;     // Get interrupt vector.
  interrupt_function = (void(*)())vector;
  (*interrupt_function)();  // Call vectored interrupt function.
}

/*************************************************************************
 * Function Name: fiq_handler
 * Parameters: void
 * Return: void
 *
 * Description: FIQ subroutine
 *		
 *************************************************************************/
__fiq __arm void fiq_handler (void)
{
void (*interrupt_function)();
unsigned int vector;

  vector = VICVectAddr;     // Get interrupt vector.
  interrupt_function = (void(*)())vector;
  (*interrupt_function)();  // Call vectored interrupt function.
}



int main (void)
{ 
  void motorspeed(int, int);
  void i2c_init(void);
  int delay(int);
  
  i2c_init();
  
  while(1)
  {
    motorspeed(0x02,200);
  }
  
}


int delay(int sekund)
{
   int i=0;
   int k=0;
   for(i=0;i<sekund;i++)
   {
     for(k=0;k<sekund;k++){}
   }
   return 0;
}

void i2c_init(void)
{
  
  
  
  //pin configuration
  
  //I2C0 SDA0 and SCL0
  PINSEL0_bit.P0_3=0x01;
  PINSEL0_bit.P0_2=0x01;
  //LED pins
  IO0DIR_bit.P0_10 = 1;
  IO0DIR_bit.P0_11 = 1;
   
   
   
   
   //clear all
  I2C0CONCLR_bit.AAC=1;
  I2C0CONCLR_bit.I2ENC=1;
  I2C0CONCLR_bit.SIC=1;
  I2C0CONCLR_bit.STAC=1;
  //i2c init 
   I2C0CONSET_bit.I2EN=1;
  I2C0CONSET_bit.STA=0;
  I2C0CONSET_bit.STO=0;
  I2C0CONSET_bit.SI=0;
  I2C0CONSET_bit.AA=0;
  
  
  //set i2c rate
  I2C0SCLL_bit.COUNT=0x0004;
  I2C0SCLH_bit.COUNT=0x0004;
  return;
}

void motorspeed(int motoraddress, int speed)
{
  int datacount=1;
  int datapointer=0;
  int databuffer[8]={0,0,0,0,0,0,0,0};
  int databytes=1;
  int slaveaddr=0x02;//7-bit slave address +R/W bit
  int k=0;
  
  slaveaddr=motoraddress;
  databuffer[0]=speed;
 
  I2C0CONSET_bit.STA=1;
  while(datacount)
  {
    
    switch(I2C0STAT_bit.STATUS)
    {
    case 0x08:
      //A Start condition has been transmitted. The Slave Address + R/W bit will be transmitted,
      //an ACK bit will be received.
      
      //1. Write Slave Address with R/W bit to I2DAT.
      I2C0DAT_bit.DATA=slaveaddr;
      //2. Write 0x04 to I2CONSET to set the AA bit.
      I2C0CONSET=0x04;
      //3. Write 0x08 to I2CONCLR to clear the SI flag.
      I2C0CONCLR=0x08;
      //4. Set up Master Transmit mode data buffer.
      //5. Set up Master Receive mode data buffer.
      //6. Initialize Master data counter.
      datacount=databytes;
      break;
      
    case 0x18:
      //Previous state was State 8 or State 10, Slave Address + Write has been transmitted, ACK
      //has been received. The first data byte will be transmitted, an ACK bit will be received.
      
      //1. Load I2DAT with first data byte from Master Transmit buffer.
      I2C0DAT_bit.DATA=databuffer[datapointer];
      //2. Write 0x04 to I2CONSET to set the AA bit.
      I2C0CONSET=0x04;
      //3. Write 0x08 to I2CONCLR to clear the SI flag.
      I2C0CONCLR=0x08;
      //4. Increment Master Transmit buffer pointer.
      datapointer++;
      break;
      
    case 0x20:
      //Slave Address + Write has been transmitted, NOT ACK has been received. A Stop
      //condition will be transmitted.
      
      //1. Write 0x14 to I2CONSET to set the STO and AA bits.
      I2C0CONSET=0x14;
      //2. Write 0x08 to I2CONCLR to clear the SI flag.
      I2C0CONCLR=0x08;
      break;
      
    case 0x28:
      //Data has been transmitted, ACK has been received. If the transmitted data was the last
      //data byte then transmit a Stop condition, otherwise transmit the next data byte.
      
      //1. Decrement the Master data counter, skip to step 5 if not the last data byte.
      datacount--;
      if(datacount==0)
      {
        //2. Write 0x14 to I2CONSET to set the STO and AA bits.
        I2C0CONSET=0x14;
        //3. Write 0x08 to I2CONCLR to clear the SI flag.
        I2C0CONCLR=0x08;
        //4. Exit
        break;
      }
      else
      {
        //5. Load I2DAT with next data byte from Master Transmit buffer.
        I2C0DAT_bit.DATA=databuffer[datapointer];
        //6. Write 0x04 to I2CONSET to set the AA bit.
        I2C0CONSET=0x04;
        //7. Write 0x08 to I2CONCLR to clear the SI flag.
        I2C0CONCLR=0x08;
        //8. Increment Master Transmit buffer pointer
        datapointer++;
        break;
      }
      
    case 0x30:
      //Data has been transmitted, NOT ACK received. A Stop condition will be transmitted.
      
      //1. Write 0x14 to I2CONSET to set the STO and AA bits.
      I2C0CONSET=0x14;
      //2. Write 0x08 to I2CONCLR to clear the SI flag.
      I2C0CONCLR=0x08;
      break;
    case 0x38:
      //Arbitration has been lost during Slave Address + Write or data. The bus has been
      //released and not addressed Slave mode is entered. A new Start condition will be
      //transmitted when the bus is free again.
      
      //1. Write 0x24 to I2CONSET to set the STA and AA bits.
      I2C0CONSET=0x24;
      //2. Write 0x08 to I2CONCLR to clear the SI flag.
      I2C0CONCLR=0x08;
      break;

    default:
      //1. Write Slave Address with R/W bit to I2DAT.
      I2C0DAT_bit.DATA=slaveaddr;
      //2. Write 0x04 to I2CONSET to set the AA bit.
      I2C0CONSET=0x04;
      //3. Write 0x08 to I2CONCLR to clear the SI flag.
      I2C0CONCLR=0x08;
      //4. Set up Master Transmit mode data buffer.
      //5. Set up Master Receive mode data buffer.
      //6. Initialize Master data counter.
      datacount=databytes;
      break;
    }
  }
   if(k<10000)
  {
     IO0PIN_bit.P0_10 = 1;
     IO0SET_bit.P0_10 = 1;
  }
  else
  {
  IO0PIN_bit.P0_10 = 0;
  IO0SET_bit.P0_10 = 0;
  }
  if(k>30000) k=0;
  k++;
  }

 
    