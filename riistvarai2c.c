/*************************************************************************
 *
 *    Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2003
 *
 *    File name   : main.c
 *    Description : Define main module
 *
 *    History :
 *    1. Date        : Feb 10, 2005
 *       Author      : Stanimir Bonev
 *       Description : Create
 *
 * Buttons function
 * Butt1 - Next
 * Butt2 - Select
 *
 * Terminal settings:
 * UART0
 * ------------------
 * Communication speed: 9600 bps
 * Number of data bits:    8
 * Number of stop bits:    1
 * Parity:              none
 *
 * UART1
 * ------------------
 * Communication speed: 9600 bps
 * Number of data bits:    8
 * Number of stop bits:    1
 * Parity:              none
 *
 * Jumper settings:
 * ----------------
 *
 *
 * Set PWM/DAC to DAC.
 * Set Ext/J-Linck to Ext.
 *
 *
 *
 *    $Revision: 47021 $
**************************************************************************/
#include "includes.h"
#include <user_func.h>

#define LIGHT_ALWAYS_OFF  0
#define LIGHT_AUTO        1
#define LIGHT_ALWAYS_ON   2
#define LIGHT_AUTO_OFF_TO TICK_PER_SECOND*10 /* 10sec */

#define GET_TIME          '1'
#define GET_DATE          '2'

const char UART_Menu[] = "\r\nUart commands\r\n'1' Get Time\r\n'2' Get Date\r\n'?' Help\n\r";

volatile int TickSysFlag = 0;
int SysTimeUpdateFlag = 0;
int SysAlarmFlag = 0;

int B1_Short = 0;
int B2_Short = 0;

int TimeFormat = 2;
int DataFormat = 1;

int LightMode = LIGHT_AUTO;

/*************************************************************************
 * Function Name: irq_handler
 * Parameters: void
 * Return: void
 *
 * Description: IRQ subroutine
 *		
 *************************************************************************/
__irq __arm void irq_handler (void)
{
void (*interrupt_function)();
unsigned int vector;

  vector = VICVectAddr;     // Get interrupt vector.
  interrupt_function = (void(*)())vector;
  (*interrupt_function)();  // Call vectored interrupt function.
}

/*************************************************************************
 * Function Name: fiq_handler
 * Parameters: void
 * Return: void
 *
 * Description: FIQ subroutine
 *		
 *************************************************************************/
__fiq __arm void fiq_handler (void)
{
void (*interrupt_function)();
unsigned int vector;

  vector = VICVectAddr;     // Get interrupt vector.
  interrupt_function = (void(*)())vector;
  (*interrupt_function)();  // Call vectored interrupt function.
}

/*************************************************************************
 * Function Name: NonVectISR
 * Parameters: void
 * Return: void
 *
 * Description: non vectored callback subroutine
 *		
 *************************************************************************/
void NonVectISR(void)
{
}

/*************************************************************************
 * Function Name: ClearFlag
 * Parameters: void
 * Return: void
 *
 * Description: clear arg
 *		
 *************************************************************************/
void ClearFlag (void* arg)
{
int* pFlag = arg;
  *pFlag = 0;
}

/*************************************************************************
 * Function Name: DelayResolution100us
 * Parameters: Int32U Delay
 * Return: void
 *
 * Description: Timer1 CH0 subroutine - delay [100us]
 *		
 *************************************************************************/
void DelayResolution100us(Int32U Delay)
{
volatile int Flag = 1;
  // Stop Timer 1
  TIMER_Stop(TIMER1);
  // Stop Reset Timer 1 counter
  TIMER_Reset(TIMER1);
  // Set action of match module CH0
  TIMER_SetMatchAction(TIMER1, CH0, TimerAction_Interrupt | TimerAction_StopTimer,
  Delay usec_T1*100, ClearFlag, (void *)&Flag, DONOTHING);
  // Start Timer 1
  TIMER_Start(TIMER1);
  // Wait expire of delay
  while(Flag);
}

/*************************************************************************
 * Function Name: SysInit
 * Parameters: void
 * Return: int
 *
 * Description: Hardware initialize
 *		
 *************************************************************************/
int SysInit(void)
{
  // Initialize the system
#ifdef FLASH
  if (SYS_Init(FOSC, FCCLK, VPBDIV1, USER_FLASH, 0x0001FF0F,0x87FE01F1,0,0xFFFFFFFF))
    return 1;
#else
  if (SYS_Init(FOSC, FCCLK, VPBDIV1, USER_RAM,   0x0001FF0F,0x87FE01F1,0,0xFFFFFFFF))
    return 1;
#endif
  // Initialize Serial Interface
  if (UART_Init(UART0))
    return 1;
  if (UART_Init(UART1))
    return 1;

  // Initialize Timers
  PM_OpenPeripheral(PC_TIMER0);
  if (TIMER_Init(TIMER0, TIMER_PRECISION))
    return 1;
  PM_OpenPeripheral(PC_TIMER1);
  if (TIMER_Init(TIMER1, TIMER_PRECISION))
    return 1;

  // Initialize RTC
  if (RTC_Init(0))	
    return 1;

  // initialize VIC
  VIC_Init();
  VIC_SetProtectionMode(UserandPrivilegedMode);
  // Enable interrupts non vectored interrupts
  VIC_EnableNonVectoredIRQ(NonVectISR);

  // UART0 interrupt
  VIC_SetVectoredIRQ(UART0_ISR,VIC_Slot0,VIC_UART0);
  VIC_EnableInt(1<<VIC_UART0);

  // UART1 interrupt
  VIC_SetVectoredIRQ(UART1_ISR,VIC_Slot1,VIC_UART1);
  VIC_EnableInt(1<<VIC_UART1);

  // Timer0 interrupt
  VIC_SetVectoredIRQ(TIMER0_ISR,VIC_Slot2,VIC_TIMER0);
  VIC_EnableInt(1<<VIC_TIMER0);

  // Timer1 interrupt
  VIC_SetVectoredIRQ(TIMER1_ISR,VIC_Slot3,VIC_TIMER1);
  VIC_EnableInt(1<<VIC_TIMER1);

  // UART1 interrupt
  VIC_SetVectoredIRQ(UART1_ISR,VIC_Slot4,VIC_UART1);
  VIC_EnableInt(1<<VIC_UART1);

  // RTC interrupt
  VIC_SetVectoredIRQ(RTC_ISR,VIC_Slot5,VIC_RTC);
  VIC_EnableInt(1<<VIC_RTC);

  UART_PutStringByPolling(UART0, "\n\rLPC2148 Hardware Init Finish!\n\r");
  UART_PutStringByPolling(UART0, "======Start User Program======\n\r");
  UART_PutStringByPolling(UART1, "\n\rLPC2148 Hardware Init Finish!\n\r");
  UART_PutStringByPolling(UART1, "======Start User Program======\n\r");
  return 0;
}

/*************************************************************************
 * Function Name: UserStart
 * Parameters: void
 * Return: void
 *
 * Description: User demo subroutine
 *		
 *************************************************************************/
void UserStart(void)
{
  /* System time init */
  TIMER_SetMatchAction(TIMER0, CH0, TimerAction_Interrupt | TimerAction_ResetTimer,
  1sec_T0/TICK_PER_SECOND, SetSysTickFlag, (void *)&TickSysFlag, DONOTHING);
  TIMER_Start(TIMER0);
  /* HD44780 LCD driver init */
  HD44780_PowerUpInit();
  /* Buttons Init */
  ButtonsInit();
  /* Light Init */
  LightInit();
  /* Menu Init */
  MenuInit(MENU_ENGLISH_LANG,MENU_WELCOME_SHOW,NULL,MENU_WELCOM_DLY);
  /* RTC_Enable */
  RTC_Enable();
}

/*************************************************************************
 * Function Name: main
 * Parameters: void
 * Return: void
 *
 * Description: Main subroutine
 *		
 *************************************************************************/
void i2c_init(void)
{
  
  
  
  //pin configuration
  
  //I2C0 SDA0 and SCL0
  PINSEL0_bit.P0_3=0x01;
  PINSEL0_bit.P0_2=0x01;
  //LED pins
  IO0DIR_bit.P0_10 = 1;
  IO0DIR_bit.P0_11 = 1;
   
   
   
   
   //clear all
  I2C0CONCLR_bit.AAC=1;
  I2C0CONCLR_bit.I2ENC=1;
  I2C0CONCLR_bit.SIC=1;
  I2C0CONCLR_bit.STAC=1;
  //i2c init 
   I2C0CONSET_bit.I2EN=1;
  I2C0CONSET_bit.STA=0;
  I2C0CONSET_bit.STO=0;
  I2C0CONSET_bit.SI=0;
  I2C0CONSET_bit.AA=0;
  
  
  //set i2c rate
  I2C0SCLL=8;
  I2C0SCLH=8;
  return;
}

void i2c0_op(int SLA, int data)
{
  unsigned char dataBuffer [8] = {0,0,0,0,0,0,0,0};
  unsigned char slaveAddress = SLA;
  unsigned char datacount;
  unsigned char status;
  //start condition
  I2C0CONSET = 0x20;
  
  dataBuffer[0] = data;
  datacount = 1;
  while(datacount)
  {
    
    status = I2C0STAT;
    switch(status)
    {
    case 0x08: //start saadetud, saadan slave aadressi + Write biti. j�rgmine state 0x18 v�i 0x40
      I2C0DAT = slaveAddress << 1;
      I2C0CONCLR = 0x20;        //ei saada repeeted starti
      I2C0CONCLR = 0x08;        //remove SI flag
      break;
    case 0x18: // slave aadress saadetud, saadan esimese data byte'i. j�rgmine state 0x28
      I2C0CONCLR = 0x20;        //remove start flag
      I2C0DAT = 0xAA;   //dataBuffer[0];
      
      I2C0CONCLR = 0x08; // remove SI flag
      break;
    case 0x28:
      datacount--;
      if(datacount) //saadab j�rgmise data byte'i
      {
        
      }else // data saadetud, saadab repeeted starti
      {
         I2C0CONSET = 0x20;     // saadab repeeted starti
         I2C0CONCLR = 0x08;     // remove SI flag
      }
      break;
    default:
      I2C0CONSET = 0x10;        //stop condition
      I2C0CONCLR = 0x08;        //remove SI flag
      break;
    }
  }
}

int main (void)
{
  int i;
  i2c_init();
  
  /*
  IO0DIR_bit.P0_2 = 1;
  IO0DIR_bit.P0_3 = 1;
  IO0CLR_bit.P0_2 = 1;
  IO0CLR_bit.P0_3 = 1;
  */
     i2c0_op(0x68, 0x1D);
  for(i=0; i<1000; i++);
     i2c0_op(0x68, 0x1D);
 
     
}

