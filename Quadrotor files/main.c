

#include "definitions.h"
#include "includes.h"
#include "imu.h"

int main (void)
{
  
  
//declaration of acce and gyro arrays
extern float acce_zero_values[3];      //Array for each acce axis zero value
extern float gyro_zero_values[3];       //Array for each gyro axis zero value
extern float AcceDataSmoothed[3];	//Array for each acce axis smoothed data
extern float AcceAngle[3];	        //Array for each acce axis angle
extern float GyroDataSmoothed[3];       //Array for each gyro axis smoothed data
extern float current_speed[4];
extern float AcceValues[3][5];

//declaration of timer & sensor values
extern int flight_time;
extern int currentHeight;

//declaration of PID values
extern float iState_x;
extern float iState_y;
extern float last_x;
extern float last_y;


//Let's give zero values:
//PID
iState_x = 0.0;
iState_y = 0.0;
last_x = 0.0;
last_y = 0.0;

//Timer, sensor & current speed
flight_time = 0;
currentHeight = 0;
for (int i=0; i<4; i++) { current_speed[i] = 0; }

//initialize
  i2c1_init();
  i2c0_init();
  acce_init();
  gyro_init();
  getIMUzeroValues();
  timer_init();
  start_timer0();
  start_timer1();
  
  while(1)set_motor_rpm1(REAR_M,20);
  
  //TEST for motors
  int speed = 1;
  /*
  while(1){
  motors(speed,speed,speed,speed);
  speed = (speed <= 70)? speed++ : 70;
  //set_motor_rpm(RIGHT_M,20);
  //calculateAllAngles();
  }
  */
  //while(1){set_motor_rpm1(FRONT_M,20);}
  //Let's fly (play the Airwolf theme)
  
  //P_sonar_rise(10000,2);
  //stabilize(800, 100);
  //P_sonar_land(1);
}
