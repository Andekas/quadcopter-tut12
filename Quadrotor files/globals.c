/*

This file combines all global functions.

 */

// Insert sort. From "whistler" - http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1283456170/0
void isort(int *a, int n){
  for (int i = 1; i < n; ++i){
    int j = a[i];
    int k;
    for (k = i - 1; (k >= 0) && (j < a[k]); k--){
      a[k + 1] = a[k];
    }
    a[k + 1] = j;
  }
}

//Finds median
int findMedian(int *data,int arraySize){
  isort(data, arraySize);

  return data[arraySize/2];
}

//Calculates square from input
float sq(n){
	return n*n;
}

//Combines MSB & LSB
int addBytes(unsigned char MSB, unsigned char LSB){
	return MSB << 8 | LSB;
}

int addBytesAcce(unsigned char MSB, unsigned char LSB){
        int value;
        if(LSB==0)value = -MSB;
        else value = 255 - MSB;
        
        return value;
}

//TODO Replace with better filter?
// Low pass filter
float filter(float currentData, float previousData, float smoothFactor){
  if (smoothFactor != 1.0) //only apply time compensated filter if smoothFactor is applied
    return (previousData * (1.0 - smoothFactor) + (currentData * smoothFactor));
//return currentData;
  else
    return currentData; //if smoothFactor == 1.0, do not calculate, just bypass!
}
