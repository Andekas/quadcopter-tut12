                                                                     
                                                                     
                                                                     
#include "includes.h"                                             
#define WINDUP_GUARD_GAIN 500.0 		//some constant that limits iTerm from getting too big or small
float pTerm, iTerm, dTerm; 			//P, I and D part of PID control

float iState_x;
float iState_y;
float last_x;
float last_y;
float PID_value_x, PID_value_y;

float mootor1, mootor2, mootor3, mootor4; 	//variables for motor speeds, they depend on how the gyro is positioned on the copter, x axis has motors 1 and 3
extern float current_speed[4];

	             				//the 2 PID functions should change the motors' speeds in relation to the current speed

float error;         				//difference from the ideal position
float windupGuard;   				//for windupguard for iTerm

void PID_x_telg(float ang_x, float deltaTime, float pgain, float igain, float dgain)
{
   
    error = 0 - ang_x;                             //error is basically target - the current position, but the target is 0 in this case
    pTerm = pgain * error;			//calculates the P part of PID
    
    iState_x += error * deltaTime;
    
    windupGuard = WINDUP_GUARD_GAIN / igain;    //checks if it passes the windupguard condition
    if (iState_x > windupGuard)
    	iState_x = windupGuard;
    else if (iState_x < -windupGuard)
    	iState_x = -windupGuard;
    iTerm = igain * iState_x;			//calculates the integral part of PID
    
    dTerm = (dgain * (ang_x - last_x)) / deltaTime; //calculates the derivative part
    
    last_x = ang_x;				
    
    PID_value_x = pTerm + iTerm - dTerm;	//calculates the whole PID value
      
    if (current_speed[2] > 200 || current_speed[3] > 200) {
    
      if (PID_value_x < 0) {
        current_speed[2] = current_speed[2] + PID_value_x; 	//assigns values to motor speeds
      } else {
        current_speed[3] = current_speed[3] - PID_value_x;
      }
      
    } else {
      
      if (PID_value_x < 0) {
        current_speed[2] = current_speed[2] - PID_value_x; 	//assigns values to motor speeds
      } else {
        current_speed[3] = current_speed[3] + PID_value_x;
      }
    
    }
    
    if (current_speed[2] < 0) current_speed[2] = 0;
    if (current_speed[2] > MOTOR_MAX_SPEED) current_speed[2] = MOTOR_MAX_SPEED;
    if (current_speed[3] < 0) current_speed[3] = 0;
    if (current_speed[3] > MOTOR_MAX_SPEED) current_speed[3] = MOTOR_MAX_SPEED;

}    

void PID_y_telg(float ang_y, float deltaTime, float pgain, float igain, float dgain)
{
    
    error = -ang_y;
    pTerm = pgain * error;
    
    //iState_y += error * deltaTime;
    
    windupGuard = WINDUP_GUARD_GAIN / igain;

    if (iState_y > windupGuard)
    	iState_y = windupGuard;
    else if (iState_y < -windupGuard)
    	iState_y = -windupGuard;
    iTerm = igain * iState_y;
    
    dTerm = (dgain * (ang_y - last_y)) / deltaTime;
    
    last_y = ang_y;
    
    PID_value_y = pTerm + iTerm - dTerm;
       
    if (current_speed[0] > 200 || current_speed[1] > 200) {
    
      if (PID_value_y < 0) {  
        current_speed[0] = current_speed[0] + PID_value_y;
      } else {
        current_speed[1] = current_speed[1] - PID_value_y;
      }
      
    } else {
      if (PID_value_y < 0) {  
        current_speed[0] = current_speed[0] - PID_value_y;
      } else {
        current_speed[1] = current_speed[1] + PID_value_y;
      }
    }

    if (current_speed[0] < 0) current_speed[0] = 0;
    if (current_speed[0] > MOTOR_MAX_SPEED) current_speed[0] = MOTOR_MAX_SPEED;
    if (current_speed[1] < 0) current_speed[1] = 0;
    if (current_speed[1] > MOTOR_MAX_SPEED) current_speed[1] = MOTOR_MAX_SPEED;
    
    

}    
