#include "includes.h"

int flight_time = 0; //current flight time in ms

void timer_init()
/*
Timer counts from 0 to 4608, then sets 1 to T0IR registers bit 0 (whole register would be 0x01) (this is basically an interrupt),
then the timer continues from 4608 and counts to 9216 and sets 1 to T0IR registers bit 1(whole register would be 0x02),
then the timer continues from 9216 and counts to 18432 and sets 1 to T0IR registers bit 2(whole register would be 0x04),
then the timer continues from 18432 and counts to 36864 and sets 1 to T0IR registers bit 3(whole register would be 0x08)
and restarts from 0.

These are not ms or anything. The timer counts PCLK cycles.
In the timer example that Lauri posted it was said that when counting to 36864 the timer resets every 10 ms,
but I'm not sure if it is correct.
*/
{
  //Timer0 init
	T0TC = 0;              //Clear timercounter
  T0CTCR = 0x00;        //timer mode, every rising PCLK edge increments PC
  T0PR = 0x00000000;   //Prescale - TC is incremented, when PC reaches this value
  // match registers
  //T0MR0 = 4608;          
  //T0MR1 = 9216;
  //T0MR2 = 18432;
  T0MR3 = 3686;        //count up to this value (36864 - 100Hz???)
  
  //T0MCR_bit.MR0INT = 1; //When T0TC reaches T0MR0 value, then T0IR register bit 0 is set to 1 (interrupt)
  //T0MCR_bit.MR1INT = 1; //When T0TC reaches T0MR1 value, then T0IR register bit 1 is set to 1 (interrupt)
  //T0MCR_bit.MR2INT = 1; //When T0TC reaches T0MR2 value, then T0IR register bit 2 is set to 1 (interrupt)
  T0MCR_bit.MR3INT = 1; //When T0TC reaches T0MR3 value, then T0IR register bit 3 is set to 1 (interrupt)
  T0MCR_bit.MR3RES = 1; //When T0TC reaches T0MR3 value, then T0TC is reset
  
  T0TCR_bit.CE = 1;     //enable timer
  //we can use the T0IR (interrupt register) to check, if it is time to read sensors or send data to motors etc
  //Timer1 init

  T1TC = 0;              //Clear timercounter
  T1CTCR = 0x00;        //timer mode, every rising PCLK edge increments PC
  T1PR = 0x00000000;   //Prescale - TC is incremented, when PC reaches this value
     // match registers
   
  T1MR3 = 30000;        //count up to this value (36864 - 100Hz???)


  T1MCR_bit.MR3INT = 1;
  T1MCR_bit.MR3RES = 1;

       //enable timer
  //one can always read either T0PC value or T0TC value to get the current timer value 
}
void start_timer0()
{
	T0TCR_bit.CE = 1;
}
void start_timer1()
{
	T1TCR_bit.CE = 1;
}

void count_ms()
{
    if(T0IR_bit.MR3INT == 1)
    {
      flight_time++;
      T0IR_bit.MR3INT = 1;
    }
}
