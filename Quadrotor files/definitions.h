/*
This is a global file for all main definitions - global variables, constants, etc...

Please add definitions necessary to you here also.
 */

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

//IMU constants
#define LOOPCOUNT 42
#define SMOOTH_FACTOR 0.8
#define ACCEGYRO_FACTOR 0.96


//Sensor addresses; 
#define GYRO_ADDR 0x68
#define ACCEL_ADDR 0x53
#define SONAR_ADDR 0x70

//Accelometer (X, Y & Z) register addresses
#define ACCEL_X 0x32
#define ACCEL_Y 0x34
#define ACCEL_Z 0x36
#define ACCEL_BW_RATE 0x2C
#define ACCEL_POWER_CTL  0x2D
#define ACCEL_DATA_FORMAT   0x31

//Gyro (X, Y & Z) register addresses
#define GYRO_X 0x1D
#define GYRO_Y 0x1F
#define GYRO_Z 0x21
#define GYRO_DLPF 0x16
#define GYRO_SRD 0x15  
  
//Sonar register addresses
#define SONAR_ADDR 0x70
#define SONAR_COM 0x00 
#define SONAR_DATA 0x02

//Axis definitions, to make reading arrays more clear
#define XAXIS 0
#define YAXIS 1
#define ZAXIS 2


//Gyro & acce constants
#define GYRO_SENSITIVITY 14.375 
   //From datasheet - sensitivity of 14.375 LSBs per �/sec

//PID
#define DELTATIME 10 
   //Time between each cycle


//MOTORS
#define MOTOR_MAX_SPEED 220

#endif /* DEFINITIONS_H_ */
