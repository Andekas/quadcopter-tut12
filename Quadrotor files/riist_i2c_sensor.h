/*
Header file for riist_i2c_sensor.c
 */

#ifndef RIIST_I2C_SENSOR_H_
#define RIIST_I2C_SENSOR_H_

//Includes
#include "definitions.h" //Let's load all the constants
#include "globals.h"

//Declaration of functions
void i2c0_write(int, int, int);
int i2c0_read(unsigned char, unsigned char, int, int);

#endif /* RIIST_I2C_SENSOR_H_ */
