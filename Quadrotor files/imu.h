/*
Header file for imu.c
 */

#ifndef IMU_H_
#define IMU_H_

//Declaration of functions
void getIMUzeroValues(); //Reads & calculates accelometer & gyro zero values
void updateGyro(); //Function that updates gyro values (reads & calculates).
void updateAcce(); //Function that updates accelometer values (reads & calculates). First, each reading is processed for real data and after that, the angle for each axis is calculated based on all three readings.
float calculateAngle(int); // Calculates real angle based on gyro & acce readings. Returns value for one specified angle.
void calculateAllAngles(); //Calculates all real angles based on gyro & acce readings. Returns an array of all angles, named currentAngle[];
void getHeight(); //Gets current height in cm, adds to global variable int currentHeight


#endif /* IMU_H_ */
