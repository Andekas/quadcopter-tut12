/*
Header file for timer.c
 */

#ifndef TIMER_H_
#define TIMER_H_




//Includes
#include "definitions.h" //Let's load all the constants

//Declaration of functions
void timer_init(); //Initializes timer

void start_timer0();
void start_timer1();
void count_ms();
#endif /* TIMER_H_ */
