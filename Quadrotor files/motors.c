#include "includes.h"
#include <user_func.h>

void set_motor_rpm(int SLA, int data)
{
  unsigned char dataBuffer [8] = {0,0,0,0,0,0,0,0};
  unsigned char slaveAddress = SLA;
  unsigned char datacount;
  unsigned char status;
  //start condition
  I2C0CONSET = 0x20;
  
  if (data < 0) data = 0;
  if (data > MOTOR_MAX_SPEED) data = MOTOR_MAX_SPEED;

  dataBuffer[0] = data;
  datacount = 1;

  while(datacount)
  {
    status = I2C0STAT;
    switch(status)
    {
    case 0x08: //start saadetud, saadan slave aadressi + Write biti. j�rgmine state 0x18 v�i 0x40
      I2C0DAT = slaveAddress;
      I2C0CONCLR = 0x20;        //ei saada repeeted starti
      I2C0CONCLR = 0x08;        //remove SI flag
      while(I2C0STAT==status);
      break;
    case 0x18: // slave aadress saadetud, saadan esimese data byte'i. j�rgmine state 0x28
      I2C0CONCLR = 0x20;        // start flag maha
      I2C0DAT = dataBuffer[0];
      //while(I2C0DAT != 0xAA);
      I2C0CONCLR = 0x08; //  SI flag maha
      break;
    case 0x28:
      datacount--;
      if(datacount) //saadab j�rgmise data byte'i
      {
        I2C0CONCLR = 0x20;      //start flag maha
        I2C0DAT = 0X33;
        I2C0CONCLR = 0x08;      //SI flag maha
      }else // data saadetud, saadab repeeted starti
      {
         I2C0CONSET = 0x10;     // saadab stop conditioni
         I2C0CONCLR = 0x08;     // remove SI flag
      }
      break;
    case 0x20://not ack recieved
       I2C0CONSET = 0x10;       //saadab stop conditioni
       I2C0CONSET = 0x20;       //saadab uuesti start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
      case 0x30://
       I2C0CONSET = 0x10;       //saadab stop conditioni
       I2C0CONSET = 0x20;       //saadab uuesti start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
    case 0x38:
       I2C0CONSET = 0x20;       //saadab start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
   
    }
  }
}

void set_motor_rpm1(int SLA, int data)
{
  unsigned char dataBuffer [8] = {0,0,0,0,0,0,0,0};
  unsigned char slaveAddress = SLA;
  unsigned char datacount;
  unsigned char status;
  //start condition
  I2C1CONSET = 0x20;
  
  if (data < 0) data = 0;
  if (data > MOTOR_MAX_SPEED) data = MOTOR_MAX_SPEED;

  dataBuffer[0] = data;
  datacount = 1;

  while(datacount)
  {
    status = I2C1STAT;
    switch(status)
    {
    case 0x08: //start saadetud, saadan slave aadressi + Write biti. j�rgmine state 0x18 v�i 0x40
      I2C1DAT = slaveAddress;
      I2C1CONCLR = 0x20;        //ei saada repeeted starti
      I2C1CONCLR = 0x08;        //remove SI flag
      while(I2C1STAT==status);
      break;
    case 0x18: // slave aadress saadetud, saadan esimese data byte'i. j�rgmine state 0x28
      I2C1CONCLR = 0x20;        // start flag maha
      I2C1DAT = dataBuffer[0];
      //while(I2C1DAT != 0xAA);
      I2C1CONCLR = 0x08; //  SI flag maha
      break;
    case 0x28:
      datacount--;
      if(datacount) //saadab j�rgmise data byte'i
      {
        I2C1CONCLR = 0x20;      //start flag maha
        I2C1DAT = 0X33;
        I2C1CONCLR = 0x08;      //SI flag maha
      }else // data saadetud, saadab repeeted starti
      {
         I2C1CONSET = 0x10;     // saadab stop conditioni
         I2C1CONCLR = 0x08;     // remove SI flag
      }
      break;
    case 0x20://not ack recieved
       I2C1CONSET = 0x10;       //saadab stop conditioni
       I2C1CONSET = 0x20;       //saadab uuesti start conditioni
       I2C1CONCLR = 0x08;       //SI flag maha
      break;
      case 0x30://
       I2C1CONSET = 0x10;       //saadab stop conditioni
       I2C1CONSET = 0x20;       //saadab uuesti start conditioni
       I2C1CONCLR = 0x08;       //SI flag maha
      break;
    case 0x38:
       I2C1CONSET = 0x20;       //saadab start conditioni
       I2C1CONCLR = 0x08;       //SI flag maha
      break;
   
    }
  }
}
void motors(int front, int rear, int left, int right)
{
        int minspeed = 20;
	if (front <= 20) {
          minspeed = 0.2*MOTOR_MAX_SPEED;
          front = (int)minspeed;
        }
	if (front > MOTOR_MAX_SPEED) front = MOTOR_MAX_SPEED;
	if (rear <= 20) {
          minspeed = 0.2*MOTOR_MAX_SPEED;
          rear = (int)minspeed;
        }
	if (rear > MOTOR_MAX_SPEED) rear = MOTOR_MAX_SPEED;
        if (left <= 20) {
          minspeed = 0.2*MOTOR_MAX_SPEED;
          left = (int)minspeed;
        }
	if (left > MOTOR_MAX_SPEED) left = MOTOR_MAX_SPEED;
	if (right <= 20) {
          minspeed = 0.2*MOTOR_MAX_SPEED;
          right = (int)minspeed;
        }
        if (right > MOTOR_MAX_SPEED) right = MOTOR_MAX_SPEED;
        if(COPTER==4 || COPTER==2)
        {
	set_motor_rpm(FRONT_M,front);
	set_motor_rpm(REAR_M,rear);
	set_motor_rpm(LEFT_M,left);
	set_motor_rpm(RIGHT_M,right);
        }
        else
        {
        //set_motor_rpm1(FRONT_M,0);
	//set_motor_rpm1(REAR_M,0);
	set_motor_rpm1(LEFT_M,left);
	set_motor_rpm1(RIGHT_M,right);
        }
}
