/*
Header file for flight.c
 */

#ifndef FLIGHT_H_
#define FLIGHT_H_

//Declaration of functions
void P_sonar_rise(int,int); //Function to rise. Variables passed are target height (cm) and speed
void P_sonar_land(float); //Function to land. Variable passed is speed.
void stabilize(int,int); //Function to stay on current position. Variable passed is time in ms.

#endif /* FLIGHT_H_ */
