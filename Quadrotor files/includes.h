/***************************************************************************
 **
 **    Master inlude file
 **
 **    Used with ARM IAR C/C++ Compiler and Assembler.
 **
 **    (c) Copyright IAR Systems 2005
 **
 **    $Revision: 47021 $
 **
 ***************************************************************************/

#ifndef  __INCLUDES_H
#define  __INCLUDES_H

#include    "flight.h"
#include    "motors.h"
#include    "definitions.h"
#include    "user_definitions.h"
#include    "globals.h"
#include    "imu.h"
#include    "init.h"
#include    "riist_i2c_sensor.h" //TODO hiljem �ldine i2c
#include    "PID.h"
#include	"timer.h"

#include    <stdio.h>
#include    <string.h>
#include    <ctype.h>
#include    <stdlib.h>
#include    <stdbool.h>
#include	<math.h>

#include    <intrinsics.h>
#include    <nxp/iolpc2148.h>

#include    "LPC_data_type.h"
#include    "arm_comm.h"
#include    "board.h"

#include    "menu_cnfg.h"
#include    "menu.h"
#include    "drv_hd44780.h"
#include    "LPC_SysControl.h"
#include    "LPC_Vic.h"
#include    "LPC_Uart.h"
#include    "LPC_Timer.h"
#include    "LPC_Rtc.h"

#endif // __INCLUDES_H
