/*
Header file for PID.c
 */

#ifndef PID_H_
#define PID_H_

//Includes
#include "definitions.h" //Let's load all the constants


//Declaration of functions
void PID_x_telg(float, float, float, float, float);
void PID_y_telg(float, float, float, float, float);

#endif /* PID_H_ */
