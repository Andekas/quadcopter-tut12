#include "includes.h"


void i2c1_init(void)
{
  
  
  
  //pin configuration
  
  //I2C1 SDA0 and SCL0
  PINSEL0_bit.P0_11=0x03;
  PINSEL0_bit.P0_14=0x03;
  //LED pins
  IO0DIR_bit.P0_10 = 1;
  IO0DIR_bit.P0_11 = 1;
   
   
   
   
   //clear all
  I2C1CONCLR_bit.AAC=1;
  I2C1CONCLR_bit.I2ENC=1;
  I2C1CONCLR_bit.SIC=1;
  I2C1CONCLR_bit.STAC=1;
  //i2c init 
   I2C1CONSET_bit.I2EN=1;
  I2C1CONSET_bit.STA=0;
  I2C1CONSET_bit.STO=0;
  I2C1CONSET_bit.SI=0;
  I2C1CONSET_bit.AA=0;
  
  
  //set i2c rate
  I2C1SCLL=8;
  I2C1SCLH=8;
  return;
}
void i2c0_init(void)
{
  
  
  
  //pin configuration
  
  //I2C1 SDA0 and SCL0
  PINSEL0_bit.P0_3=0x01;
  PINSEL0_bit.P0_2=0x01;
  //LED pins
  IO0DIR_bit.P0_10 = 1;
  IO0DIR_bit.P0_11 = 1;
   
   
   
   
   //clear all
  I2C0CONCLR_bit.AAC=1;
  I2C0CONCLR_bit.I2ENC=1;
  I2C0CONCLR_bit.SIC=1;
  I2C0CONCLR_bit.STAC=1;
  //i2c init 
   I2C0CONSET_bit.I2EN=1;
  I2C0CONSET_bit.STA=0;
  I2C0CONSET_bit.STO=0;
  I2C0CONSET_bit.SI=0;
  I2C0CONSET_bit.AA=0;
  
  
  //set i2c rate
  I2C0SCLL=8;
  I2C0SCLH=8;
  return;
}
void acce_init()
{
	i2c0_write(ACCEL_ADDR, ACCEL_BW_RATE, 0x0A);
	i2c0_write(ACCEL_ADDR, ACCEL_POWER_CTL, 0x08);
	i2c0_write(ACCEL_ADDR, ACCEL_DATA_FORMAT, 0x00);
}
void gyro_init()
{
        i2c0_write(GYRO_ADDR, GYRO_DLPF, 0x1B);
	i2c0_write(GYRO_ADDR, GYRO_SRD, 0x07);
}  
void sonar_init()
{
    i2c0_write(SONAR_ADDR, SONAR_COM, 0x51);
}

