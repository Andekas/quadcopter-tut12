/*
 * user_definitions.h
 *
 *  Created on: 10.12.2012
 *      Author: Ande
 */


#ifndef USER_DEFINITIONS_H_
#define USER_DEFINITIONS_H_

#define COPTER 1

//TEAM 1
//Motor addresses
#define FRONT_M 0x52
#define REAR_M 0x56
#define LEFT_M 0x58
#define RIGHT_M 0x54
#define RISE_SPEED 180
   
//PID
#define PGAIN_X 0.8
#define IGAIN_X 0.0000002
#define DGAIN_X 0.0000005
#define PGAIN_Y 0.8
#define IGAIN_Y 0.0000002
#define DGAIN_Y 0.0000005

   
//Motor addresses team 2
/*
#define FRONT_M 0x56
#define REAR_M 0x52
#define LEFT_M 0x58
#define RIGHT_M 0x54
*/   

#endif /* USER_DEFINITIONS_H_ */
