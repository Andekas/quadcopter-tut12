#include "globals.h"
#include "includes.h"
void i2c0_write(int SLA, int RA, int data)
{
  unsigned char dataBuffer [2] = {0,0};
  unsigned char slaveAddress = SLA;
  unsigned char datacount;
  unsigned char status;
  //start condition
  I2C0CONSET = 0x20;
  
  dataBuffer[0] = RA;
  dataBuffer[1] = data;
  datacount = 2;
  while(datacount)
  {
  //uuesti:
    status = I2C0STAT;
    switch(status)
    {
    case 0x08: //start saadetud, saadan slave aadressi + Write biti. j�rgmine state 0x18 v�i 0x40
      I2C0DAT = slaveAddress << 1;
      I2C0CONCLR = 0x20;        //ei saada repeeted starti
      I2C0CONCLR = 0x08;        //remove SI flag
      while(I2C0STAT==status);
      break;
    case 0x18: // slave aadress saadetud, saadan esimese data byte'i. j�rgmine state 0x28
      I2C0CONCLR = 0x20;        // start flag maha
      I2C0DAT = dataBuffer[0];
      //while(I2C0DAT != 0xAA);
      I2C0CONCLR = 0x08; //  SI flag maha
      break;
    case 0x28:
      datacount--;
      if(datacount) //saadab j�rgmise data byte'i
      {
        I2C0CONCLR = 0x20;      //start flag maha
        I2C0DAT = dataBuffer[1];
        I2C0CONCLR = 0x08;      //SI flag maha
      }else // data saadetud, saadab repeeted starti
      {
         I2C0CONSET = 0x10;     // saadab stop conditioni
         I2C0CONCLR = 0x08;     // remove SI flag
      }
      break;
    case 0x20://not ack recieved
       I2C0CONSET = 0x10;       //saadab stop conditioni
       I2C0CONSET = 0x20;       //saadab uuesti start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
      case 0x30://
       I2C0CONSET = 0x10;       //saadab stop conditioni
       I2C0CONSET = 0x20;       //saadab uuesti start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
    case 0x38:
       I2C0CONSET = 0x20;       //saadab start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
    //case 0x40://slave aadress ja R_bit saadetud ootab esimest data byte'i j�rgmine state: 0x50 v�i 0x58
      
      
      break;
      /*
    default: 
      goto uuesti;
      I2C0CONSET = 0x10;        //stop condition
      I2C0CONCLR = 0x08;        //remove SI flag
      break;
      */
    }
  }
}
int i2c0_read(unsigned char SLA, unsigned char RA, int databytes, int firstbyte)
//Params are device address, index, bytes to read and is the first byte returned MSB (1) or LSB (0)
{
  unsigned char dataBuffer [8] = {0, 0, 0, 0, 0, 0, 0, 0};
  //unsigned char slaveAddress = SLA;
  unsigned char status;
  int dataCount = 0;
  if(!databytes)databytes = 2;
  //start condition
  I2C0CONSET = 0x20;
  
  
  while(dataCount < databytes)
  {
  //uuesti:
    status = I2C0STAT;
    switch(status)
    {
    case 0x08: //start saadetud, saadan slave aadressi + Write biti. j�rgmine state 0x18
      I2C0DAT = SLA << 1;
      I2C0CONCLR = 0x20;        //ei saada repeeted starti
      I2C0CONCLR = 0x08;        // SI flag maha
      //while(I2C0STAT==status);
      break;
    case 0x18: // slave aadress saadetud, saadan registri aadressi. j�rgmine state 0x28
      I2C0CONCLR = 0x20;        // start flag maha
      
      I2C0DAT = RA;
      //while(I2C0DAT != 0xAA);
      I2C0CONCLR = 0x08; //  SI flag maha
      break;
    case 0x28: //registri aadress saadetud, saadan repeeted starti, j�rgmine state 0x10
        I2C0CONSET = 0x20;      //start flag (saadab repeeted starti)
        I2C0CONCLR = 0x08;      //SI flag maha
      break;
    case 0x10:  //repeeted start saadetud, saadan slave aadressi ja read byte'i,  j�rgmine state 0x40
        I2C0DAT = SLA << 1 |1;     //slaveaddress + read bit
        I2C0CONCLR = 0x20;      //start flag maha
        I2C0CONCLR = 0x08;      //SI flag maha
        I2C0CONSET = 0x04;      //AA flag
      break;
    case 0x20://not ack recieved
       I2C0CONSET = 0x10;       //saadab stop conditioni
       I2C0CONSET = 0x20;       //saadab uuesti start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
      case 0x30://
       I2C0CONSET = 0x10;       //saadab stop conditioni
       I2C0CONSET = 0x20;       //saadab uuesti start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
    case 0x38:
       I2C0CONSET = 0x20;       //saadab start conditioni
       I2C0CONCLR = 0x08;       //SI flag maha
      break;
    case 0x40:  //slave aadress ja R_bit saadetud ootab esimest data byte'i j�rgmine state: 0x50 v�i 0x58
      if(dataCount < databytes - 1){        //kui on veel lugeda rohkem kui 1 byte
        I2C0CONSET = 0x04;      //AA flag (saadab datale vastu acknowledge'i) j�rgmine state 0x50
      }else{
        I2C0CONCLR = 0x04;      //AA flag maha (saadab datale vastu not acknowledge'i) j�rgmine state 0x58
      }
      I2C0CONCLR = 0x08;        //SI flag maha
      break;
    case 0x50:  //data byte saadud
      dataBuffer[dataCount] = I2C0DAT;        //j�tab data byte'i meelde
      dataCount++;                 //v�hendab loetavata byte'ide arvu
      if(dataCount < databytes - 1){        //kui on veel lugeda rohkem kui 1 byte
        I2C0CONSET = 0x04;      //AA flag (saadab datale vastu acknowledge'i)
      }else{
        I2C0CONCLR = 0x04;      //AA flag maha (saadab datale vastu not acknowledge'i)
      }
      I2C0CONCLR = 0x08;        //SI flag maha
      break;
    case 0x58:  //viimane data byte saadud
      dataBuffer[dataCount] = I2C0DAT;        //j�tad data byte'i meelde
      dataCount++;
      I2C0CONSET = 0x10;        //stop condition
      I2C0CONCLR = 0x08;        //SI flag maha
      break;
      /*
    default: 
      goto uuesti;
      I2C0CONSET = 0x10;        //stop condition
      I2C0CONCLR = 0x08;        //remove SI flag
      break;
      */
    }
  } // -while
  if (firstbyte == 0){
	  return addBytesAcce(dataBuffer[0], dataBuffer[1]);
  } else {
	  return addBytes(dataBuffer[0], dataBuffer[1]);
  }
  
}
