/*
IMU stands for Inertial Measurements Unit

This file combines all functions necessary directly for gyro & acce (and possibly, magnetometer later).

 */

   
#include "includes.h" //With that, we should include all files

//Just in case, the files we actually need:
#include "definitions.h" //Let's load all the constants
#include "riist_i2c_sensor.h" //For reading the values
#include "timer.h" //Timer
#include "imu.h"
   //global acce and gyro arrays
 float acce_zero_values[3] ;
 float AcceDataSmoothed[3];
 float gyro_zero_values[3];
 float GyroDataSmoothed[3];
 float AcceAngle[3];
 float currentAngle[3];
 int currentHeight;
 int AcceValues[3][5];
 
// AcceValues = {0,0,0,0,0};

void getIMUzeroValues(){
	/*Reads & calculates accelometer & gyro zero values (when there's no movement, for calibration).
	The zero walues will be in raw data, because I think that each calculation removes some of the accuracy. */

	// Take 42 measurements of all 3 axis, find the median, that's our zero-point
	// Why 42? Because that's the answer to life, the universe, and everything - http://bit.ly/teMaOF

	int findZero[LOOPCOUNT];
        int kontroll;
	//Zero values for acce

	//Loop for each axis
	for (char axis = XAXIS; axis <= ZAXIS; axis++){
		//And loop for each measurement
		for (int i=0; i<LOOPCOUNT; i++){
			//Each measurement will be added to findZero array
			findZero[i] = i2c0_read(ACCEL_ADDR, ACCEL_X + (axis * 2),2,0);
                        kontroll=findZero[i];
                }
		//All zero values (median from measurements) will be added to acce_zero_values[X,Y,Z] array.
		//Also, let's give initial values to AcceDataSmoothed array
		acce_zero_values[axis] = findMedian(findZero, LOOPCOUNT);
                AcceDataSmoothed[axis] = 0;
	}

	//Zero values for gyro

	//Loop for each axis
	for (char axis = XAXIS; axis <= ZAXIS; axis++){
		//And loop for each measurement
		for (int i=0; i<LOOPCOUNT; i++){
			//Each measurement will be added to findZero array
			findZero[i] = i2c0_read(GYRO_ADDR, GYRO_X + (axis * 2),2,1);
		}
		//All zero values (median from measurements) will be added to gyro_zero_values[X,Y,Z] array.
		//Also, let's give initial values to GyroDataSmoothed array
		gyro_zero_values[axis] = GyroDataSmoothed[axis] = findMedian(findZero, LOOPCOUNT);
	}
    //return gyro_zero_values[0];
}

void updateGyro(){
	//Function that updates gyro values (reads & calculates).

	// Updates all raw measurements from the gyroscope

	//Define the variables
	float dataRaw[3];		//Array for each axis raw data

	//Read the acce measurements
	for (int axis = XAXIS; axis <= ZAXIS; axis++) {
		dataRaw[axis] = (i2c0_read(GYRO_ADDR, GYRO_X + (axis * 2),2,1) - gyro_zero_values[axis]);
		GyroDataSmoothed[axis] = dataRaw[axis] / GYRO_SENSITIVITY;
	}
}

void updateAcce(){
	/*Function that updates accelometer values (reads & calculates).
	First, each reading is processed for real data and after that,
	the angle for each axis is calculated based on all three readings. */

	// Updates all raw measurements from the accelerometer

	//Define the variables
         float dataRaw[3] = {0,0,0};		//Array for each axis raw data
        
	//Read and smooth the acce measurements
	for (int axis = XAXIS; axis <= ZAXIS; axis++) {
		dataRaw[axis] = (i2c0_read(ACCEL_ADDR, ACCEL_X + (axis * 2),2,0) - acce_zero_values[axis]);
                
                AcceValues[axis][0] = AcceValues[axis][1];
                AcceValues[axis][1] = AcceValues[axis][2];
                AcceValues[axis][2] = AcceValues[axis][3];
                AcceValues[axis][3] = AcceValues[axis][4];
                AcceValues[axis][4] = dataRaw[axis];
                
                int AcceValuesTemp[5]; 
               
                for (int i = 0; i < 5; i++) {
                 AcceValuesTemp[i] = AcceValues[axis][i];
                } 
                
                dataRaw[axis] = findMedian(AcceValuesTemp, 5);
                
		AcceDataSmoothed[axis] = filter(dataRaw[axis], AcceDataSmoothed[axis], SMOOTH_FACTOR);
	}

	//Calculates accelometer X, Y, Z angles.
	//* 57.296 is for conversion from radians to degrees
	//AcceAngle[XAXIS] = atan2(AcceDataSmoothed[XAXIS], sqrt( sq(AcceDataSmoothed[YAXIS]) + sq(AcceDataSmoothed[ZAXIS]) ) ) * 57.296;
	//AcceAngle[YAXIS] = atan2(AcceDataSmoothed[YAXIS], sqrt( sq(AcceDataSmoothed[XAXIS]) + sq(AcceDataSmoothed[ZAXIS]) ) ) * 57.296;
	//AcceAngle[ZAXIS] = atan2(sqrt( sq(AcceDataSmoothed[XAXIS]) + sq(AcceDataSmoothed[YAXIS]) ), AcceDataSmoothed[ZAXIS] ) * 57.296;
        AcceAngle[XAXIS] = AcceDataSmoothed[XAXIS]*90/255;
        AcceAngle[YAXIS] = AcceDataSmoothed[YAXIS]*90/255;
        AcceAngle[ZAXIS] = AcceDataSmoothed[ZAXIS]*90/255;


}

float calculateAngle(int axis) {
	//Calculates real angle based on gyro & acce readings. Returns value for one specified angle.
	//TODO needs testing - do we need timer (dT), should we use previous readings etc.

	updateGyro();
	updateAcce();

	float requested_angle;

	if(isnan(AcceAngle[axis])) AcceAngle[axis] = 0;
	if(isnan(GyroDataSmoothed[axis])) GyroDataSmoothed[axis] = 0;

	requested_angle = (ACCEGYRO_FACTOR * GyroDataSmoothed[axis]) + ((1-ACCEGYRO_FACTOR) * AcceAngle[axis]);
	return requested_angle;
}

void calculateAllAngles() {
	updateGyro();
        updateAcce();
    for (int axis = XAXIS; axis <= ZAXIS; axis++) {

    	if(isnan(AcceAngle[axis])) AcceAngle[axis] = 0;
    	if(isnan(GyroDataSmoothed[axis])) GyroDataSmoothed[axis] = 0;

      //  currentAngle[axis] = (ACCEGYRO_FACTOR * GyroDataSmoothed[axis]) + ((1-ACCEGYRO_FACTOR) * AcceAngle[axis]);
        currentAngle[axis] = AcceAngle[axis];
    }
}

void getHeight(){
  /*sonar_init();
  int reading = 0;
  reading = i2c0_read(SONAR_ADDR, SONAR_DATA, 2,1);
  if (reading != 255) {
	  currentHeight = reading;
  }*/
  currentHeight = 10;
}
