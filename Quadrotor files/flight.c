/*

This file combines all functions necessary directly for flying.

 */

#include "includes.h" //With that, we should include all files

float current_speed[4];
extern int currentHeight;
extern float currentAngle[];

void P_sonar_rise(int target,int speed) //function that's meant to be run in a loop before PID controls for x and y axis, korgus is the current height
{
  getHeight();
  target = currentHeight + 500;
  
  while (current_speed[0] < (RISE_SPEED - 30)){
    
    //if (T1IR_bit.MR3INT == 1){

        for(int i = 0; i<4; i++) {
                current_speed[i] += speed;
        }

        motors((int)current_speed[0],(int)current_speed[1],(int)current_speed[2],(int)current_speed[3]);

       // T1IR_bit.MR3INT = 1;
    //}
    
  }
  

	getHeight();
  target = currentHeight + 500;

	//First, let's rise as close to target as possible.
	while(currentHeight < target || currentHeight > (target + 20)) {

		calculateAllAngles();
                
                int previousHeight;
                previousHeight = currentHeight;

	//	if (T1IR_bit.MR3INT == 1){

			PID_x_telg(currentAngle[XAXIS], DELTATIME, PGAIN_X, IGAIN_X, DGAIN_X);
			PID_y_telg(currentAngle[YAXIS], DELTATIME, PGAIN_Y, IGAIN_Y, DGAIN_Y);
                  
                  /*current_speed[3] = current_speed[3] + currentAngle[0]*2; 
                  current_speed[2] = current_speed[2] - currentAngle[0]*2; 
                  
                  
                  if (current_speed[0] > 180 || current_speed[1] > 180 ) {
                  
                    if (currentAngle[1]<0){
                      current_speed[1] = current_speed[1] + currentAngle[1]/6; 
                    } else {
                      current_speed[0] = current_speed[0] - currentAngle[1]/6; 
                    }
                    
                  } else {
                    
                    if (currentAngle[1]<0){
                      current_speed[0] = current_speed[0] - currentAngle[1]/6; 
                    } else {
                      current_speed[1] = current_speed[1] + currentAngle[1]/6; 
                    }
                    
                  }*/
                    
                      if (current_speed[0] < (MOTOR_MAX_SPEED) && current_speed[1] < (MOTOR_MAX_SPEED) && current_speed[2] < (MOTOR_MAX_SPEED) && current_speed[3] < (MOTOR_MAX_SPEED)){

			for(int i = 0; i<4; i++) {
				current_speed[i] += speed;
			}
                      
                  }
                  
                /*if (current_speed[0] < 0) current_speed[0] = 0;
                if (current_speed[0] > MOTOR_MAX_SPEED) current_speed[0] = MOTOR_MAX_SPEED;
                if (current_speed[1] < 0) current_speed[1] = 0;
                if (current_speed[1] > MOTOR_MAX_SPEED) current_speed[1] = MOTOR_MAX_SPEED;*/

			motors((int)current_speed[0],(int)current_speed[1],(int)current_speed[2],(int)current_speed[3]);

	//		T1IR_bit.MR3INT = 1;
	//	}

		getHeight();
                target = currentHeight + 500;
                
                if (currentHeight > target + 60){
                  currentHeight = previousHeight;
                }
	}

	// I'm afraid that we might never get exact height and stay on a endless loop.
	//To avoid that, we are trying to correct our positions for limited time.
	int rise_stabilization_factor = 42; //We need a variable to define loop cycles.
	int i = 0;
        
        while(1){
        motors(0,0,0,0);
        }

	while (i<rise_stabilization_factor){

		calculateAllAngles();

		if (T1IR_bit.MR3INT == 1){

			getHeight();

			PID_x_telg(currentAngle[XAXIS], DELTATIME, PGAIN_X, IGAIN_X, DGAIN_X);
			PID_y_telg(currentAngle[YAXIS], DELTATIME, PGAIN_Y, IGAIN_Y, DGAIN_Y);

			for(int i = 0; i<4; i++) {
				if(currentHeight<target) {
					current_speed[i] += speed;
				} else if (currentHeight > target){
					current_speed[i] -= speed;
				}
			}

			motors((int)current_speed[0],(int)current_speed[1],(int)current_speed[2],(int)current_speed[3]);

			T1IR_bit.MR3INT = 1;
			i++;

		}

	}

	//And now, the height should be pretty much accurate. We have completed our rise!
}


void P_sonar_land(float speed)
{
	while(current_speed[0] > 0 || current_speed[1] > 0 || current_speed[2] > 0 || current_speed[3] > 0) {

		calculateAllAngles();

		if (T1IR_bit.MR3INT == 1){

			PID_x_telg(currentAngle[XAXIS], DELTATIME, PGAIN_X, IGAIN_X, DGAIN_X);
			PID_y_telg(currentAngle[YAXIS], DELTATIME, PGAIN_Y, IGAIN_Y, DGAIN_Y);

			for(int i = 0; i<4; i++) {
				current_speed[i] -= speed;
			}

			motors((int)current_speed[0],(int)current_speed[1],(int)current_speed[2],(int)current_speed[3]);

			T1IR_bit.MR3INT = 1;
		}

	}

	motors(0,0,0,0);

   //TODO play a little victory-tune

}

void stabilize(int time, int height){

	int endTime;
	extern int flight_time;

	int speed = 1;
        
	endTime = flight_time + time;

	while (endTime > flight_time){
                count_ms();
		calculateAllAngles();

		if (T1IR_bit.MR3INT == 1){
                  
                        PID_x_telg(currentAngle[XAXIS], DELTATIME, PGAIN_X, IGAIN_X, DGAIN_X);
			PID_y_telg(currentAngle[YAXIS], DELTATIME, PGAIN_Y, IGAIN_Y, DGAIN_Y);

			for(int i = 0; i<4; i++) {
				if(currentHeight<height) {
					current_speed[i] += speed;
				} else if (currentHeight > height){
					current_speed[i] -= speed;
				}
			}

			motors((int)current_speed[0],(int)current_speed[1],(int)current_speed[2],(int)current_speed[3]);

			T1IR_bit.MR3INT = 1;

		}
	}
}
