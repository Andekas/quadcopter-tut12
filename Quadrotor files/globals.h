/*
Header file for globals.c
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

//Includes
#include "definitions.h" //Let's load all the constants


//Declaration of functions
void isort(int *, char);
int findMedian(int *, char);
float sq(float);
int addBytes(unsigned char, unsigned char);
float filter(float, float, float); //Low pass filter

#endif /* GLOBALS_H_ */
